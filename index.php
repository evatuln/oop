<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');
 
    $Shaun = new Animal("Shaun");
    echo "Nama : ".$Shaun->nama. "<br>";
    echo "Legs : " .$Shaun->kaki. "<br>";
    echo "Cool Blooded : " .$Shaun->darah. "<br>";
    echo "<br>";
    echo "<br>";

    $Kodok = new Animal("Frog");
    echo "Nama : ".$Kodok->nama. "<br>";
    echo "Legs : " .$Kodok->kaki. "<br>";
    echo "Cool Blooded : " .$Kodok->darah. "<br>";
    echo $Kodok->Jump();
    echo "<br>";
    echo "<br>";

    $Sunggokong = new Animal("Ape");
    echo "Nama : ".$Sunggokong->nama. "<br>";
    echo "Legs : " .$Sunggokong->kaki. "<br>";
    echo "Cool Blooded : " .$Sunggokong->darah. "<br>";
    echo $Sunggokong->Yell();
    echo "<br>";
    echo "<br>";
    




// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>